## Archer's Path

[Play Online](http://mssite.org/projects/ArchersPath/)

Edge library uses "kedge" branch.
Several methods from thx.core have been removed to be able to compile with Haxe 4.

### About
* Tileset: ansimuz (CC-BY 3.0)
* Characters art: Calciumtrice (CC-BY 3.0) (color edits by Merry Dream Games)
* Lifebar: LokiF (No Copyright)
* Level Editor icons: Freepik.com
* Some graphics have my changes.
* Music: Stewart Copeland
* Made with [Kha](https://github.com/Kode/Kha)
